# Carte au trésors
## Démarrage de l'application
Executer la main classe MainCarteAuTresors.java

## Saisie des données
Pour saisir les données, il faut intéragir avec la console I/O

- Exemple du chemin vers le fichier de la carte : C:\Desktop\carte.txt
- Exemple du chemin vers le fichier des aventuriers : C:\Desktop\aventuriers.txt
- Exemple du chemin vers le fichier des resultats : C:\Desktop\resultats.txt

## Execution de la partie
Après la saisie des données, la partie commence. 
Dans la console I/O vous pouvez suivre l'état de la carte à chaque seconde :

```
      -   -   -   -   -   - 
    | O | J | O | O | O | O | 
      -   -   -   -   -   - 
    | O | O | O | 1 | O | O | 
      -   -   -   -   -   - 
    | O | O | O | O | X | O | 
      -   -   -   -   -   - 
    | 3 | O | O | O | O | O | 
      -   -   -   -   -   - 
    | O | O | O | O | O | O | 
      -   -   -   -   -   - 
    Seconde :2 Seconde :2 Seconde :2 Seconde :2 Seconde :2
```
où :
- 0 : Case vide
- X : Montagne
- un nombre (exemple 3) : Trésor
- une lettre (exemple J) : Aventurier dont le nom commence par J

## Résultats
Les résultats sont stockés dans le fichier indiquer lors de la saisie des données sous la forme suivante:
```
John 1-1  2-1  3-1  3-1  3-2  3-2  2-2  2-2  2-3 score : 0
```

Chaque ligne contient le nom de l'aventurier, suivi de l'ensemble des positions occupées à chaque seconde, et le nombre des trésors ramassés




