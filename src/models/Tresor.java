package models;

public class Tresor extends Case{

    private int nbTresors = 0;
    public Tresor(int nbTresors){
        super();
        this.nbTresors = nbTresors;
    }

    public int getNbTresors() {
        return nbTresors;
    }

    public void setNbTresors(int nbTresors) {
        this.nbTresors = nbTresors;
    }

    @Override
    public String toString() {
        return ""+nbTresors;
    }
}
