package models;

import java.util.ArrayList;
import java.util.List;

public class Aventurier extends Case {
    private String nom;
    private int x;
    private int y;
    private char orientation;
    private int nbTresors = 0;
    private int aRamasser = 0;
    private List<Mouvement> actions = new ArrayList<>();
    private List<Position> historiquePositions = new ArrayList<>();

    public Aventurier(String nom, int x, int y, char orientation, String mouvements){
        this.nom = nom;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.initializeActions(mouvements);
        this.addPositionToHistory();
    }

    public char getOrientation() {
        return orientation;
    }
    public void setOrientation(char orientation){
        this.orientation = orientation;
    }

    public int getX() {
        return x;
    }

    public void setX(int x){
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y){
        this.y = y;
    }

    public int getNbTresors() {
        return nbTresors;
    }

    public void setNbTresors(int nbTresors) {
        this.nbTresors = nbTresors;
    }

    public List<Mouvement> getActions() {
        return actions;
    }

    public int getaRamasser() {
        return aRamasser;
    }

    public void setaRamasser(int aRamasser) {
        this.aRamasser = aRamasser;
    }

    public List<Position> getHistoriquePositions() {
        return historiquePositions;
    }

    public String getNom() {
        return nom;
    }

    public void addPositionToHistory(){
        historiquePositions.add(new Position(this.x+1, this.y+1));
    }

    public void ramasse(Tresor tresor){
        this.aRamasser = tresor.getNbTresors();
        tresor.setNbTresors(0);
    }

    public Mouvement getNextMouvement() {
        if (actions.size() > 0) {
            return actions.remove(0);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return nom.substring(0, 1).toUpperCase();
    }

    private void initializeActions(String mouvements){
        for(char ch: mouvements.toCharArray()){
            switch(ch){
                case 'A':
                    actions.add(Mouvement.AVANCE);
                    break;
                case 'D':
                    actions.add(Mouvement.DROITE);
                    break;
                case 'G':
                    actions.add(Mouvement.GAUCHE);
                    break;
            }
        }
    }
}
