package models;

public class Carte {
    private int xMax;
    private int yMax;
    private Case[][] map;

    public int getxMax() {
        return xMax;
    }

    public int getyMax() {
        return yMax;
    }

    public void setxMax(int xMax) {
        this.xMax = xMax;
    }

    public void setyMax(int yMax) {
        this.yMax = yMax;
    }

    public Case[][] getMap() {
        return map;
    }

    public void setMap(Case[][] map) {
        this.map = map;
    }

    public void print(){
        for(int j = 0; j < yMax; j++){
            for(int i = 0; i < xMax; i++){
                System.out.print("  - ");
            }
            System.out.println();
            System.out.print("| ");
            for(int i = 0; i < xMax; i++){
                System.out.print(map[i][j] == null ? "O" : map[i][j]);
                System.out.print(" | ");
            }
            System.out.println();
        }
        for(int i = 0; i < xMax; i++){
            System.out.print("  - ");
        }
        System.out.println();
    }
}
