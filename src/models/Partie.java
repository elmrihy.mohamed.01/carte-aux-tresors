package models;

import java.util.ArrayList;
import java.util.List;

public class Partie {
    private Carte carte;
    private List<Aventurier> aventurier = new ArrayList<>();

    public Carte getCarte() {
        return carte;
    }

    public List<Aventurier> getAventurier() {
        return aventurier;
    }

    public void setCarte(Carte carte) {
        this.carte = carte;
    }

    public void setAventurier(List<Aventurier> aventurier) {
        this.aventurier = aventurier;
    }
}
