import models.Aventurier;
import models.Carte;
import models.Partie;
import services.PartieService;
import java.util.Scanner;

public class MainCarteAuTresors {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Saisir le chemin vers le fichier de la carte: ");
        String cheminCarte = sc.nextLine();
        System.out.println("Saisir le chemin vers le fichier decrivant les aventuriers: ");
        String cheminAventuriers = sc.nextLine();
        System.out.println("Saisir le chemin vers le fichier où vous voulez avoir les résultat: ");
        String resultatChemin = sc.nextLine();
        sc.close();

        Partie partie = PartieService.creerPartie(cheminCarte, cheminAventuriers);
        PartieService.run(partie);
        PartieService.getResultat(partie, resultatChemin);
    }
}
