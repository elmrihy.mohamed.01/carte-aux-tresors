package services;

import models.Carte;
import models.Case;
import models.Montagne;
import models.Tresor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CarteService {

    private CarteService(){

    }
    public static Carte creerCarteFichier(String path){
        BufferedReader reader;
        try {
            Carte carte = new Carte();
            reader = new BufferedReader(new FileReader(
                    path));
            String line = reader.readLine();
            while (line != null && !line.isEmpty()) {
                construireCarte(carte,line);
                line = reader.readLine();
            }
            reader.close();
            return carte;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    //il faut ajouter des exceptions
    private static void construireCarte(Carte carte, String line){
        String[] elements = line.split(" ");
        String[] coordonnes = elements[1].split("-");
        if(line.startsWith("T")){
            carte.getMap()[Integer.parseInt(coordonnes[0])-1][Integer.parseInt(coordonnes[1])-1] = new Tresor(Integer.parseInt(elements[2]));
        }else if(line.startsWith("M")){
            carte.getMap()[Integer.parseInt(coordonnes[0])-1][Integer.parseInt(coordonnes[1])-1] = new Montagne();
        }else if(line.startsWith("C")){
            carte.setxMax(Integer.parseInt(elements[1]));
            carte.setyMax(Integer.parseInt(elements[2]));
            carte.setMap(new Case[carte.getxMax()][carte.getyMax()]);
        }
    }
}
