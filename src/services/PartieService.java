package services;

import models.Aventurier;
import models.Carte;
import models.Partie;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class PartieService {
    private PartieService(){

    }

    public static Partie creerPartie(String fichierCarte, String fichierAventuriers){
        Partie partie = new Partie();
        Carte carte = CarteService.creerCarteFichier(fichierCarte);
        List<Aventurier> listeAventuriers = AventurierService.creerAventuriersFichier(fichierAventuriers);
        mettreAventurierSurCarte(carte, listeAventuriers);
        partie.setAventurier(listeAventuriers);
        partie.setCarte(carte);
        return partie;
    }

    public static void run(Partie partie){
        int seconde = 0;
        boolean next;
        partie.getCarte().print();
        System.out.println("Seconde :"+seconde+" Seconde :"+seconde+" Seconde :"+seconde+" Seconde :"+seconde+" Seconde :"+seconde);
        do {
            next = false;
            for(Aventurier av : partie.getAventurier()) {
                AventurierService.executerMouvementSuivant(av, partie.getCarte());
                next = next || av.getActions().size() > 0;
            }
            seconde++;
            partie.getCarte().print();
            System.out.println("Seconde :"+seconde+" Seconde :"+seconde+" Seconde :"+seconde+" Seconde :"+seconde+" Seconde :"+seconde);
        }while(next);
    }

    public static void getResultat(Partie partie,String fichierResultat){
        try{
            FileWriter fileWriter = new FileWriter(fichierResultat);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            for(Aventurier aventurier : partie.getAventurier()){
                printWriter.println(AventurierService.getResultat(aventurier));
            }
            printWriter.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void mettreAventurierSurCarte(Carte carte, List<Aventurier> listeAventuriers){
        for(Aventurier av: listeAventuriers){
            carte.getMap()[av.getX()][av.getY()] = av;
        }
    }

}
