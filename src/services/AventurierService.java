package services;

import models.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AventurierService {

    private AventurierService(){

    }

    public static List<Aventurier> creerAventuriersFichier(String path){
        BufferedReader reader;
        List<Aventurier> aventuriers = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader(
                    path));
            String line = reader.readLine();
            while (line != null && !line.isEmpty()) {
                aventuriers.add(creerAventurierLigne(line));
                line = reader.readLine();
            }
            reader.close();
            return aventuriers;
        } catch (IOException e) {
            e.printStackTrace();
            return aventuriers;
        }
    }

    private static Aventurier creerAventurierLigne(String ligne){
        String[] infos = ligne.split(" ");
        String[] position = infos[1].split("-");
        return new Aventurier(infos[0], Integer.parseInt(position[0]) - 1, Integer.parseInt(position[1]) - 1, infos[2].charAt(0), infos[3]);
    }

    public static void executerMouvementSuivant(Aventurier aventurier, Carte carte){

        if(aventurier.getaRamasser() > 0){
            aventurier.setNbTresors(aventurier.getNbTresors()+aventurier.getaRamasser());
            aventurier.setaRamasser(0);
            aventurier.addPositionToHistory();
            return;
        }
        Mouvement mouvement = aventurier.getNextMouvement();
        if(mouvement == null){
            return;
        }
        switch (mouvement){
            case AVANCE:
                avancer(aventurier, carte);
                break;
            case DROITE:
                droite(aventurier);
                break;
            case GAUCHE:
                gauche(aventurier);
                break;
            default:
                break;
        }
        aventurier.addPositionToHistory();
    }

    private static void avancer(Aventurier aventurier, Carte carte){
        int newX = aventurier.getX();
        int newY = aventurier.getY();
        switch (aventurier.getOrientation()){
            case 'N':
                newY--;
                break;
            case 'S':
                newY++;
                break;
            case 'E':
                newX++;
                break;
            case 'O':
                newX--;
                break;
            default:
                return;
        }
        avancer(aventurier, carte, newX, newY);
    }
    private static void avancer(Aventurier aventurier, Carte carte, int nouveauX, int nouveauY){
        if(verifierCarteLimites(carte, nouveauX, nouveauY) && verifierCarteObstacles(carte, nouveauX, nouveauY)){
            if(carte.getMap()[nouveauX][nouveauY] instanceof Tresor){
                aventurier.ramasse((Tresor) carte.getMap()[nouveauX][nouveauY]);
            }
            carte.getMap()[nouveauX][nouveauY] = aventurier;
            carte.getMap()[aventurier.getX()][aventurier.getY()] = null;
            aventurier.setY(nouveauY);
            aventurier.setX(nouveauX);
        }
    }

    private static boolean verifierCarteObstacles(Carte carte, int x, int y){
        return !(carte.getMap()[x][y] instanceof Montagne) && !(carte.getMap()[x][y] instanceof Aventurier);
    }

    private static boolean verifierCarteLimites(Carte carte, int x, int y){
        return x>=0 && x < carte.getxMax() && y>=0 && y <carte.getyMax();
    }

    private static void droite(Aventurier aventurier){
        switch (aventurier.getOrientation()){
            case 'N':
                aventurier.setOrientation('E');
                break;
            case 'S':
                aventurier.setOrientation('O');
                break;
            case 'E':
                aventurier.setOrientation('S');
                break;
            case 'O':
                aventurier.setOrientation('N');
                break;
            default:
                break;
        }
    }

    private static void gauche(Aventurier aventurier){
        switch (aventurier.getOrientation()){
            case 'N':
                aventurier.setOrientation('O');
                break;
            case 'S':
                aventurier.setOrientation('E');
                break;
            case 'E':
                aventurier.setOrientation('N');
                break;
            case 'O':
                aventurier.setOrientation('S');
                break;
            default:
                break;
        }
    }

    public static String getResultat(Aventurier aventurier){
        String resultat = aventurier.getNom();
        for(Position position: aventurier.getHistoriquePositions()){
            resultat += " "+position.getX()+"-"+position.getY()+" ";
        }
        resultat += "score : "+aventurier.getNbTresors();
        return resultat;
    }
}
